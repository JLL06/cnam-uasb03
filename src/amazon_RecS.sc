/* Recherche par similarite sur distance Jaccard */
/* JL LAFENETRE - Septembre 2018                 */

import org.apache.spark.ml.feature.MinHashLSH
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.mllib.linalg.VectorUDT
import org.apache.spark.sql.types.{LongType, StructField, StructType}
import org.apache.spark.mllib.util.MLUtils

// Définition d'un schéma pour charger le dataframe depuis le disque
val schema = StructType(Seq(
  StructField("id", LongType, true),
  StructField("keys", new VectorUDT, true)))

// Chargement du dataframe
val dfA = spark.read.schema(schema).json("data/export_arangodb.json")

// Conversion du dataframe du formet MLLIB vers le format ML
val convertedVecDF = MLUtils.convertVectorColumnsToML(dfA)

// Recherche des plus proches voisins
val mh = new MinHashLSH()
  .setNumHashTables(5)
  .setInputCol("keys")
  .setOutputCol("values")

val model = mh.fit(convertedVecDF)

// Vecteur requete (exemple)
val key = Vectors.sparse(492799, Seq((3215, 1.0),(26882, 1.0),(43136, 1.0),(43342, 1.0),(147237, 1.0),(148547, 1.0)))

// Recherche des 5 plus proches voisins
model.approxNearestNeighbors(convertedVecDF, key, 5).show()

