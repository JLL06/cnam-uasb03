#!/usr/bin/perl
# Formatage du fichier d'export d'ArangoDB pour import dans Spark
# JLL - Septembre 2018

open(FILE,$ARGV[0]);
for $line (<FILE>) {
	$line=~s/^\[//;
	$line=~s/\]$//;
	$line=~s/\},/\}\n/g;
	$line=~s/"1\.0"/1\.0/g;
	print $line;
}
close FILE;
