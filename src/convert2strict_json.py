import json
import gzip
def parse(path):
  g = gzip.open(path, 'r')
  for l in g:
    yield json.dumps(eval(l))

f = open("output.strict", 'w')
for l in parse("meta_CDs_and_Vinyl.json.gz"):
  f.write(l + '\n')
