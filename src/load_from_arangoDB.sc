// Chargement de données depuis ArangoDB
// JL LAFENETRE - Septembre 2018
//
// spark-shell --jars arangodb-spark-connector-1.0.2.jar,arangodb-java-driver-4.3.2.jar,velocypack-1.0.14.jar,velocypack-module-jdk8-1.0.3.jar,velocypack-module-scala-1.0.1.jar,json-simple-1.1.1.jar,slf4j-api-1.7.13.jar
//
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import com.arangodb.spark.ArangoSpark

// Déclaration d'un classe pour lecture des données ArangoDB
// A copier d'un bloc
import scala.beans.BeanProperty
case class Item (@BeanProperty var name: String,
                 @BeanProperty var login: String) {
       def this() = this(null, null)
}
//////////////////////

// Chargement des données sous forme de string
val itemString = ArangoSpark.load[String](sc, "test")

// Chargement des données avec la case class => Erreur JAVA
val itemRDD = ArangoSpark.load[Item](sc, "test")

